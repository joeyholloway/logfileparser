#### Testing ####
In the folder that contains the POM file run: 

* mvn verify

#### Building ####
In the folder that contains the POM file run: 

* mvn clean package

#### Running ####
When running the executable jar you need to pass in exactly two arguments: 

* input location with the log files - note: if this location does not exist it will throw an error
* output location where you want the log files to go - note: if this location exists it will throw an error

The example below has the input and output file in the same directory as the Jar
```java -jar LogFileParser-1.0-SNAPSHOT.jar input output```

##### Adding new games #####
They can be added to the gamesList.properties file 

##### Options for improvements: #####
Remove the hardcoded values of what column contains certain information. i.e. it is assumed that the second column will always what the userID - this could make the programme fragile. 

I realise that GameInstallation and GameStart could use the same validator - this is something I would only look to optimise after the integration tests are written. 

##### Assumptions ####
UserId can be a string of all letters or numbers or both

ProductDescription can also be a string of all letters or numbers or both 