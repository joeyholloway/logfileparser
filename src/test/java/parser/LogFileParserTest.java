package parser;

import enums.CSVFileName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static enums.CSVFileName.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class LogFileParserTest {

    private LogFileParser logFileParser = new LogFileParser();

    @ParameterizedTest
    @MethodSource("eventNamesAndCSVFileNameValues")
    void itShouldReturnTheCSVFileForTheLogName(String eventName, CSVFileName csvFileName) {
        //given: the event name element in the log line - eventName param

        //when: parse event name is called
        CSVFileName fileName = logFileParser.getEventName(eventName);

        //then: an array of files should be returned
        assertEquals(fileName, csvFileName);
    }

    static Stream<Arguments> eventNamesAndCSVFileNameValues() {
        return Stream.of(
                arguments("gameinstallation", GAMEINSTALLATION),
                arguments("gamestart", GAMESTART),
                arguments("gamepurchase",  GAMEPURCHASE),
                arguments("gamesetup", INVALID)
        );
    }
}