import exceptions.IncorrectDirectorySpecified;
import exceptions.IncorrectNumberOfArguments;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class LogFileETLTest {

    @ParameterizedTest(name = "Test for incorrect number of arguments")
    @MethodSource("programmeArgsProvider")
    void itShouldThrowExceptionForIncorrectNumberOfArguments(List<String> args) {
        //when: the string array is passed to the main programme

        //then: an exception is thrown
        assertThrows(IncorrectNumberOfArguments.class, () -> {
            LogFileETL.main(args.toArray(String[]::new));
        });
    }

    static Stream<Arguments> programmeArgsProvider() {
        return Stream.of(
                arguments(Arrays.asList("/inputPathOne")),
                arguments(Arrays.asList("/inputPathOne" , "/inputPathTwo", "/inputPathThree"))
                );
    }

    @Test
    void itShouldThrowAnExceptionIfTheInputDirectoryIsNotADirectory() {
        //when: A String array with 2 values
        String[] args = new String[]{"/inputPath", "/outputPath"};

        //then; Log folder parser parse folder is called
        assertThrows(IncorrectDirectorySpecified.class, () -> {
            LogFileETL.main(args);
        });
    }

    @Test
    void itShouldThrowAnExceptionIfTheOutputDirectoryIsAlreadyADirectory() throws IOException {
        //given: two existing directories
        Path inputPath = Files.createTempDirectory("inputPath");
        Path outputPath = Files.createTempDirectory("outputPath");

        //when: A String array with 2 values
        String[] args = new String[]{inputPath.toString(), outputPath.toString()};

        //then; Log folder parser parse folder is called
        assertThrows(IncorrectDirectorySpecified.class, () -> {
            LogFileETL.main(args);
        });
    }
}