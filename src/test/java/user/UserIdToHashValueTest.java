package user;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserIdToHashValueTest {

    @Test
    void itShouldReturnTheHashValueForTheUserId() throws NoSuchAlgorithmException {
        //given: a player id
        String userId = "jane";

        //when: the method is called
        String actualResult = UserIdToHashValue.idToHashValue(userId);

        //then: the correct hash value is returned
        assertEquals("81f8f6dde88365f3928796ec7aa53f72820b06db8664f5fe76a7eb13e24546a2" , actualResult);
    }
}