package processor;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LogFileExtractorTest {

    @Test
    void itShouldReturnAllOfTheLogFilesInADirectory() throws IOException {
        //given: an input directory
        Path inputPath = Files.createTempDirectory("inputPath");

        //and: a log file
        Files.createTempFile(inputPath, "log", ".log");

        //when: the log file extractor is called
        List<Path> logFiles = LogFileExtractor.getInputLogs(inputPath);

        //then: an array with one path init is returned
        assertEquals(1, logFiles.size());
    }
}