package validators.events;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class GameInstallationTest {

    @ParameterizedTest
    @MethodSource("logLineAndExpectedValue")
    void itShouldCorrectlyValidateTheLogLine(String logLine, boolean expectedResult) {
        //given: the log line is split into a string array
        String[] splitLogLine = logLine.split("\\|");

        //when: the method is called with the log line
        boolean actualResult = GameInstallation.isLogLineValid(splitLogLine);

        //then: the correct outcome is given
        assertEquals(expectedResult, actualResult);
    }

    private static Stream<Arguments> logLineAndExpectedValue() {
        return Stream.of(
                arguments("gameinstallation|jane|1|1587583122", true),
                arguments("gameinstallation|jane|1587583122", false),
                arguments("gameinstallation|jane|4|1587583122", false),
                arguments("gameinstallation|jane|4|abde", false)
        );
    }
}