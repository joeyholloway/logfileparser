package validators;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import validators.items.Game;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class GameTest {

    @BeforeEach
    void setUp() {
    }

    @ParameterizedTest
    @MethodSource("gameIdAndResult")
    void isShouldReturnWhetherTheGameIsValid(Integer gameId, boolean expectedResult) {
        //given: the gameId

        //when: is game id valid called with the game id
        boolean actualResult = Game.isValidGameId(gameId.toString());

        //then: the result matches the expected result
        assertEquals(expectedResult, actualResult);
    }

    static Stream<Arguments> gameIdAndResult(){
        return Stream.of(
                arguments(1, true),
                arguments(2, true),
                arguments(3, true),
                arguments(4, false)
        );
    }
}