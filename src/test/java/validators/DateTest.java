package validators;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import validators.items.Date;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class DateTest {

    @ParameterizedTest
    @MethodSource("datesAndValidity")
    void itShouldTestThatTheDateIsNumeric(String date, boolean expectedOutcome) {
        //given: a string sate value

        //when: the method is called to see whether it is valid
        boolean actualOutcome = Date.isDateANumberType(date);

        //then: it should give the correct outcome
        assertEquals(actualOutcome, expectedOutcome);

    }

    private static Stream<Arguments> datesAndValidity() {
        return Stream.of(
                arguments("1587583122", true),
                arguments("abcdefg", false)
        );
    }
}