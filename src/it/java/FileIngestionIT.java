import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class FileIngestionIT {

    private static final String OUTPUT_PATH = "outputPath";

    @AfterEach
    void tearDown() throws IOException {
        deleteDirectory(OUTPUT_PATH);
    }

    @Test
    public void createOutputDirectory() throws IOException {
        //given: an input directory
        Path inputPath = Files.createTempDirectory("inputPath");

        //when: A String args array with 2 values - input and output path
        String[] args = new String[]{inputPath.toString(), OUTPUT_PATH};

        //and: the programmes main method is called with the arguments
        LogFileETL.main(args);

        //then: output folder exists
        Path path = Paths.get(OUTPUT_PATH);
        assertTrue(Files.isDirectory(path));
    }

    @ParameterizedTest
    @MethodSource("fileNameAndExpectedOutput")
    public void processLogFiles(String fileName, String expectedCSVline) throws IOException {
        //given: an input directory
        Path inputPath = Files.createTempDirectory("inputPath");

        //and: a game installation csv is in the input folder
        File file = new File("src/it/resources/"+ fileName + ".log");
        Files.copy(file.toPath(), inputPath.resolve(file.getName()));

        //when: A String args array with 2 values - input and output path
        String[] args = new String[]{inputPath.toString(), OUTPUT_PATH};

        //and: the programmes main method is called with the arguments
        LogFileETL.main(args);

        //then: output folder exists
        Path path = Paths.get(OUTPUT_PATH);
        assertTrue(Files.isDirectory(path));

        //and: gameinstallation csv file contains one file
        File gameInstallationCSV = new File(path.toString() + "/" + fileName + ".csv");
        long actualLineCount = Files.lines(gameInstallationCSV.toPath()).count();
        assertEquals(1L, actualLineCount);

        //and: it contains the correct values
        String actaulCSVLine = Files.lines(gameInstallationCSV.toPath()).collect(Collectors.joining(""));
        assertEquals(expectedCSVline, actaulCSVLine);
    }

    private static Stream<Arguments> fileNameAndExpectedOutput() {
        return Stream.of(
          arguments("gameinstallation", "\"81f8f6dde88365f3928796ec7aa53f72820b06db8664f5fe76a7eb13e24546a2\",\"1\",\"1587583122\""),
          arguments("gamestart", "\"81f8f6dde88365f3928796ec7aa53f72820b06db8664f5fe76a7eb13e24546a2\",\"2\",\"1587583122\""),
          arguments("gamepurchase", "\"81f8f6dde88365f3928796ec7aa53f72820b06db8664f5fe76a7eb13e24546a2\",\"1\",\"1 Free Life\",\"1587583122\""),
          arguments("invalid", "gamestart|jane|2|abcdefg")
        );
    }

    private static void deleteDirectory(String dir) throws IOException {
        Path path = Paths.get(dir);
        try (Stream<Path> walk = Files.walk(path)) {
            walk.sorted(Comparator.reverseOrder()).forEach(FileIngestionIT::deletePath);
        }
    }

    private static void deletePath(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            System.err.printf("Unable to delete this path : %s%n%s", path, e);
        }
    }
}