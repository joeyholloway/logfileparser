import exceptions.IncorrectDirectorySpecified;
import exceptions.IncorrectNumberOfArguments;
import processor.LogETLProcessor;

import java.io.File;
import java.io.IOException;

public class LogFileETL {

    private static LogETLProcessor logETLProcessor;

    public static void main(String[] args) throws IOException {
        if(args.length != 2){
            throw new IncorrectNumberOfArguments("Please pass in two arguments");
        }

        File inputDirectory = new File(args[0]);
        File outputDirectory = new File(args[1]);

        if(inputDirectory.isDirectory() && !outputDirectory.isDirectory()){
            logETLProcessor = new LogETLProcessor();
            logETLProcessor.process(inputDirectory, outputDirectory);

        } else if (!inputDirectory.isDirectory()) {
            throw new IncorrectDirectorySpecified("Please specify a valid input directory");
        } else if (outputDirectory.isDirectory()) {
            throw new IncorrectDirectorySpecified("Output directory already exists");
        }
    }
}