package enums;

import validators.events.GameInstallation;
import validators.events.GamePurchase;
import validators.events.GameStart;

public enum CSVFileName {
    GAMEINSTALLATION {
        @Override
        public boolean isLineValid(String[] line) {
            return GameInstallation.isLogLineValid(line);
        }
    },
    GAMEPURCHASE {
        @Override
        public boolean isLineValid(String[] line) {
            return GamePurchase.isLogLineValid(line);
        }
    },
    GAMESTART {
        @Override
        public boolean isLineValid(String[] line) {
            return GameStart.isLogLineValid(line);
        }
    },
    INVALID {
        @Override
        public boolean isLineValid(String[] line) {
            return false;
        }
    };

    public abstract boolean isLineValid(String[] line);
}