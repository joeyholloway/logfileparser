package exceptions;

public class IncorrectNumberOfArguments extends RuntimeException {
    public IncorrectNumberOfArguments(String s) {
        super(s);
    }
}
