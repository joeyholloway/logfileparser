package exceptions;

public class IncorrectDirectorySpecified extends RuntimeException {
    public IncorrectDirectorySpecified(String s) {
        super(s);
    }
}
