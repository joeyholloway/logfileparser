package writer;

import enums.CSVFileName;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class OutputWriter {

    public Map<CSVFileName, PrintWriter> createOutputWriters(File outputDirectory) throws FileNotFoundException {
        Map<CSVFileName, PrintWriter> outputWriters = new HashMap<>();
        CSVFileName[] fileNamesEnums = CSVFileName.values();
        List<CSVFileName> fileNameList = Arrays.asList(fileNamesEnums);

        for(CSVFileName fileName : fileNameList) {
            outputWriters.put(fileName, new PrintWriter(outputDirectory.toPath().toString() + "/" +fileName.name().toLowerCase() + ".csv"));
        }

        return outputWriters;
    }

    public void closeWriters(Map<CSVFileName, PrintWriter> outputWriters) {
        List<PrintWriter> printWriters = new ArrayList<>(outputWriters.values());
        printWriters.forEach(PrintWriter::close);
    }
}
