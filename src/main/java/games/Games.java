package games;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Games {

    private static Map<Integer, String> games = new HashMap<>();

    public static Map<Integer, String> getGames(){
        if(games.isEmpty()){
            loadGames();
        }
        return games;
    }

    private static void loadGames() {
        try (InputStream input = Games.class.getClassLoader().getResourceAsStream("gamesList.properties")) {
            Properties properties = new Properties();
            properties.load(input);
            games = new HashMap(properties);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
