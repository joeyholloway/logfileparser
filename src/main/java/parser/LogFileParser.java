package parser;

import enums.CSVFileName;
import user.UserIdToHashValue;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogFileParser {

    public void parseFile(Path inputPath, Map<CSVFileName, PrintWriter> outputWriters) throws IOException {
        Stream<String> lines = Files.lines(inputPath);
        lines.forEach(line ->  processLine(line, outputWriters));
    }

    private void processLine(String line,  Map<CSVFileName, PrintWriter> outputWriters){
        String[] splitLine = line.split("\\|");
        CSVFileName csvFileName = getEventName(splitLine[0]);

        if(csvFileName.isLineValid(splitLine)){
            LinkedList<String> logLine = updateOutputValues(splitLine);
            String csvLine = convertToCSV(logLine);
            outputWriters.get(csvFileName).println(csvLine);
        } else {
            outputWriters.get(CSVFileName.INVALID).println(line);
        }
    }

    private LinkedList<String> updateOutputValues(String[] splitLine) {
        LinkedList<String> logLine = new LinkedList<>(Arrays.asList(splitLine));
        logLine.remove(0);
        String userHashValue = UserIdToHashValue.idToHashValue(logLine.remove(0));
        logLine.addFirst(userHashValue);
        return logLine;
    }

    public CSVFileName getEventName(String eventName) {
        try{
           return CSVFileName.valueOf(eventName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            return CSVFileName.INVALID;
        }
    }

    private String convertToCSV(LinkedList data) {
        return (String) data.stream()
                .map(columnValue -> "\"" + columnValue + "\"")
                .collect(Collectors.joining(","));
    }
}
