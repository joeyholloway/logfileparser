package validators.items;

public class Date {

    public static boolean isDateANumberType(String date) {
        try {
            Integer.parseInt(date);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
