package validators.items;

import games.Games;

import java.util.Map;

public class Game {

    public static boolean isValidGameId(String gameId){
        Map<Integer, String> games = Games.getGames();
        return games.containsKey(gameId);
    }
}
