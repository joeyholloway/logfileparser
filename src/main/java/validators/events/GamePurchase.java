package validators.events;

import validators.items.Date;
import validators.items.Game;

public class GamePurchase {

    private static int NUMBER_OF_COLUMNS = 5;
    private static int GAME_ID_COLUMN_NUMBER = 2;
    private static int DATE_COLUMN_NUMBER = 4;

    public static boolean isLogLineValid(String[] logLine){
        if(logLine.length == NUMBER_OF_COLUMNS && Game.isValidGameId(logLine[GAME_ID_COLUMN_NUMBER]) && Date.isDateANumberType(logLine[DATE_COLUMN_NUMBER])) {
            return true;
        } else {
            return false;
        }
    }
}
