package processor;

import enums.CSVFileName;
import parser.LogFileParser;
import writer.OutputWriter;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LogETLProcessor {

    public void process(File inputDirectory, File outputDirectory) throws IOException {
        LogFileParser logFileParser = new LogFileParser();
        OutputWriter outputWriter = new OutputWriter();

        List<Path> filePaths = LogFileExtractor.getInputLogs(inputDirectory.toPath());
        createOutputDirectory(outputDirectory);
        Map<CSVFileName, PrintWriter> outputWriters = outputWriter.createOutputWriters(outputDirectory);

        for(Path path: filePaths){
            logFileParser.parseFile(path, outputWriters);
        }
        outputWriter.closeWriters(outputWriters);
    }

    private void createOutputDirectory(File outputDirectory) throws IOException {
        Files.createDirectories(outputDirectory.toPath());
    }
}